import numpy as np
from numpy import sinc, inf, zeros_like, zeros, ones_like, ones
import locale
locale.setlocale(locale.LC_ALL, 'deu_deu')
import matplotlib
matplotlib.rcParams['axes.formatter.use_locale'] = True
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import math
import ecvproc
import admproc
import mtfa

#font_family = 'serif'
font_family = 'sans'
font_size = 13
params= {'text.usetex': False,
#         'mathtext.fontset': 'stix',
         'mathtext.fontset': 'stixsans',
         'font.size': font_size,
         'axes.labelsize': font_size,
         'font.family': font_family,
         'figure.figsize': [4, 3.2],
         'legend.fontsize': font_size,
         'svg.fonttype':'none'
         }
plt.rcParams.update(params)

def sci_notation(num, decimal_digits=1, precision=None, exponent=None):
    if not exponent: 
        exponent = int(math.floor(math.log10(abs(num))))
    coeff = round(num/float(10**exponent), decimal_digits)
    if not precision:
        precision = decimal_digits
    
    return "{0:.{2}f} \\times 10^{{{1:d}}}".format(coeff, exponent, 
                                                     precision)